<?php
App::uses('AppModel', 'Model');
/**
 * Cashflow Model
 *
 */
class Cashflow extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'date' => array(
			'date' => array(
				'rule' => array('date'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'prio' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'amount' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cashflow' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'type' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'type_identifier' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public function calculateCashflow($data, $paydate) {
		//hier komt de logica om Cashflows te berekenen

		//test of we hier komen als we een invoice editeren
        //var_dump($data);
        //echo '<br>'. $paydate;

        $lastCashflowRecord = $this->find('first', array(
            'order' => array('Cashflow.id DESC'),
        ));

        //var_dump($lastCashflowRecord);

        $lastCashflowBalance = $lastCashflowRecord['Cashflow']['cashflow'];
       // echo '<br>'. $lastCashflowBalance;

        if ($data['Invoice']['invoice_type_id']=='2') {
            //Uitgaande factuur
            $newCashflowBalance = $lastCashflowBalance + $data['Invoice']['amount_incl'];
        } else {
            //inkomende factuur
            $newCashflowBalance = $lastCashflowBalance - $data['Invoice']['amount_incl'];
        }

        $this->set(array(
            'date' => $paydate,
            'prio' => '0',
            'amount' => $data['Invoice']['amount_incl'],
            'cashflow' => $newCashflowBalance,
            'type' => 'Invoice',
            'type_identifier' => '1'
        ));

        $this->save();

		//die_dump('functie calculate Cashflow wordt aangeroepen');


	}
}
