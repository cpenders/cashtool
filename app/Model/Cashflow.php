<?php
App::uses('AppModel', 'Model');
/**
 * Cashflow Model
 *
 */
class Cashflow extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'date' => array(
			'date' => array(
				'rule' => array('date'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'prio' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'amount' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cashflow' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'type' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'type_identifier' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public function calculateCashflow($data, $paydate) {

        App::import('Model','Invoice');

        $invoice = new Invoice();

        //delete alle records in Cashflow met een datum groter of gelijk aan de betaaldatum van de factuur
        //we verwijderen de records ipv ze te updaten zodat deze functie ook gebruikt kan worden bij deleten van een record.
        $this->deleteAll(array('date >=' => $paydate), false);

        //selecteer van de overgebleven record de laatst toegevoegde
        $lastCashflowRecord = $this->find('first', array(
            'order' => array('Cashflow.id DESC'),
        ));

        //haal van dit record de cashflow balans op
        $lastCashflowBalance = $lastCashflowRecord['Cashflow']['cashflow'];

        //haal alle invoices op met een paydate groter of gelijk aan de nieuwe / aangepaste record
        $invoicesToProcess = $invoice->find('all',array(
            'conditions' => array('paydate >=' => $paydate),
                'order' => array('paydate' => 'asc', 'invoice_type_id' => 'asc'))
        );

        //loop over de invoices en voeg per invoice een record toe aan Cashflow
        foreach ($invoicesToProcess as $key => $value) :
            if ($value['Invoice']['invoice_type_id']=='2') {
                //Uitgaande factuur
                $newCashflowBalance = $lastCashflowBalance + $value['Invoice']['amount_incl'];
                $amount = $value['Invoice']['amount_incl'];
            } else {
                //inkomende factuur
                $newCashflowBalance = $lastCashflowBalance - $value['Invoice']['amount_incl'];
                $amount = $value['Invoice']['amount_incl'] * (-1);
            }
            $lastCashflowBalance = $newCashflowBalance;

            //vul de waardes in voor de cashflow record
/*            $velden = array(
                'date' => $value['Invoice']['paydate'],
                'prio' => '0',
                'amount' => $amount,
                'cashflow' => $newCashflowBalance,
                'type' => 'Invoice',
                'type_identifier' => $value['Invoice']['id']
            );*/
            $this->create();

            $this->set(array(
                'date' => $value['Invoice']['paydate'],
                'prio' => '0',
                'amount' => $amount,
                'cashflow' => $newCashflowBalance,
                'type' => 'Invoice',
                'type_identifier' => $value['Invoice']['id']
            ));

            //schrijf de cashflow record weg
            $this->save();
            //deze create na de save zetten om een nieuwe Cashflow record te maken


        endforeach;

	}

    public function afterFind($results, $primary = false) {
        $red = array();
        $orange = array();

        foreach ($results as $key => $val) {
            if($val['Cashflow']['cashflow'] < 0){
                if(!in_array($val['Cashflow']['date'], $red)){
                    //in array red zetten
                    array_push($red,$val['Cashflow']['date']);
                }
                //in array red zetten
            } elseif ($val['Cashflow']['cashflow'] >= 0 && $val['Cashflow']['cashflow'] <= 50000) {
                if(!in_array($val['Cashflow']['date'], $orange)){
                    //in array orange zetten
                    array_push($orange,$val['Cashflow']['date']);
                }
            }
        }

        $new_results = array();
        foreach ($results as $key => $val) {
            //eerst testen of datum voor komt in red, daarna orange
            if(in_array($val['Cashflow']['date'],$red)) {
                $val['Cashflow']['bgcolor'] = 'red';
                

            } elseif (in_array($val['Cashflow']['date'],$orange)){
                $val['Cashflow']['bgcolor'] = 'orange';
            }

            array_push($new_results,$val);
        }

        return $new_results;
    }
}
